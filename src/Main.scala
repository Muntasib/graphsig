import java.io.File
import java.util.Scanner
import scala.collection.mutable.Map
import java.io._

/**
  * Created by mattieu on 31/03/16.
  */
object Main extends App {

  //First argument = graphs of the same format as the graphsig format
  //Second argument = ids of the graph to mine (same format as graphsig)
  var graphs:Map[String, Graph] = Map()
  var graph_id:Map[String, Graph] = Map()

  val scannerGraphs = new Scanner(new File(args(0)))
  val scannerGraphs_id = new Scanner(new File(args(1)))

  var idNode = 0
  def fillGraphs(sc: Scanner) : Map[String, Graph] = {
    var graphs:Map[String, Graph] = Map()

    while(sc.hasNext()) {
      var edges = List[Edge]()
      var nodesId = List[String]()
      var labelList = List[String]()

      val graphId = sc.next()
      val V = sc.nextInt()
      for(i <- 0 until V) {
        labelList = labelList:+ sc.next()
        nodesId = nodesId :+ idNode.toString
        idNode += 1
      }

      val E = sc.nextInt()
      for(i <- 0 until E) {
        val start = sc.nextInt()
        val end = sc.nextInt()
        val label = sc.nextInt()
        val edge1 = new Edge(nodesId(start), nodesId(end), label)
        edges = edges :+ edge1
      }

      val graph = new Graph(graphId, labelList, edges, nodesId)
      graph.buildGraph()
      graphs += (graphId -> graph)
    }
    return graphs
  }

  def getGraphs(sc: Scanner) : List[Int] = {
    var graphs = List[Int]()
    sc.nextInt()
    while(sc.hasNext()){
      graphs = graphs :+ sc.nextInt()
    }
    return graphs
  }

  def translate_fsg_format(dataset: Map[String, Graph], file_name: String): Unit = {
    val pw = new PrintWriter(new File(file_name))
    var to_write_2 = ""
    for ((k, g) <- dataset) {
      pw.write("t\n")
      for ((k2, n) <- g.graph) {
        pw.write("v " + n.id + " " + n.label + "\n")
        for (e <- n.arcs) {
          to_write_2 += "u " + n.id + " " + e.nodeEnd + " " + e.label + "\n"
        }
      }
      pw.write(to_write_2)
      to_write_2 = ""
    }
    pw.close
  }

  def translate_gspan_format(dataset: Map[String, Graph], file_name: String): Unit = {
    val pw = new PrintWriter(new File(file_name))
    var to_write_2 = ""

    var count = 0
    for ((k, g) <- dataset) {
      pw.write("t # " + count + "\n")
      count += 1
      for ((k2, n) <- g.graph) {
        pw.write("v " + n.id + " " + n.label.getBytes()(0) + "\n")
        for (e <- n.arcs) {
          to_write_2 += "e " + n.id + " " + e.nodeEnd + " " + e.label + "\n"
        }
      }
      pw.write(to_write_2)
      to_write_2 = ""
    }
    pw.close
  }

  def construct_dataset(nb: Int, file_name: String): Unit = {
    var to_write = ""
    var count = 0
    for((k, g) <- graphs) {
      if(count < nb){
        to_write += k.substring(1) + "\n"
      }
      count += 1
    }
    val pw = new PrintWriter(new File(file_name))
    pw.write(to_write)
    pw.close
  }

  def build_map_dataset(file_name: String) : Map[String, Graph] = {
    var res: Map[String, Graph] = Map()
    val sc = new Scanner(new File(file_name))
    while(sc.hasNext()){
      val id = sc.next()
      res += (id -> graphs("#" + id))
    }
    return res
  }

  graphs = fillGraphs(scannerGraphs)

  //mine active mollecules for aids dataset
  for(grId <- getGraphs(scannerGraphs_id)){
    if(graphs.contains("#" + grId.toString)) {
      graph_id("#" + grId.toString) = graphs("#" + grId.toString)
    }
  }

  val graphSig = new GraphSig(graph_id, 0.001, 0.1, 0.25, 5)
  graphSig.run()


}
