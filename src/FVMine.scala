import java.util.concurrent.CountDownLatch

import scala.collection.mutable.Map
/**
  * Created by mattieu on 1/04/16.
  */

class FVMine(minSup:Double, maxPvalue:Double, featureVectors: Map[String, Map[String, Double]]){

  var A = List[Map[String, Double]]()

  var ksI = List[String]()
  for((kn, vn) <- featureVectors) {
    for ((k,v) <- vn) {
      ksI = ksI :+ k
    }
  }
  val ks = ksI.toSet.toList

  var logFac1: List[Double] = List()
  logFac1 = logFac1 :+ 0.0
  logFac1 = logFac1 :+ 0.0
  for(i <- 2 to 10000){
    logFac1 = logFac1 :+ logFac1(i - 1) + Math.log(i)
  }

  def factorial(x: Int): Double = {
    if(x == 0) 1
    else if(x == 1) 1
    else if(x == 2) return 2
    else {
      (2 to x).reduce(_*_)
    }
  }

  def factorial(input: Int, till: Int): Int = {
    if((input to (till + 1)).size == 0) 1
    else {
      (input to (till + 1)).reduce(_*_)
    }
  }

  def C(N: Double, n: Double) : Double = {
    if(n == 0.0 || n == N){
      return 1.0
    }

    if(n > 20){
      return Math.exp(logFac(N.toInt) - logFac(n.toInt) - logFac((N - n).toInt))
    }

    if(n  > N - n){
      val n_bis = N - n
      var f = 1.0
      for(i <- (N - n_bis + 1).toInt to N.toInt){
        f *= i
      }

      for(j <- 2 to (N - n).toInt){
        f /= j
      }

      return f
    }

    var f = 1.0
    for(i <- (N - n + 1).toInt to N.toInt){
      f *= i
    }

    for(j <- 2 to n.toInt){
      f /= j
    }

    return f
    //return factorial(n.toInt)/(factorial(k.toInt) * factorial((n-k).toInt))
    //return (factorial((n-k).toInt, (n).toInt)/(factorial(k.toInt))).toDouble
    //return factorial(n.toInt, (n-k).toInt).toDouble/(factorial(k.toInt)).toDouble
  }

  def logFac(N: Int): Double = {
    if (N <= 10000){
      return logFac1(N)
    }
    return N * Math.log(N) - N
  }


  def P(k: String, vi: Double): Double = {
    var suc = 0.0
    for((k1,v) <- featureVectors) {
      if (v.getOrElse(k, 0.0) >= vi) {
        suc += 1.0
      }
    }
    return suc/featureVectors.size.toDouble
  }

  def P(v: Map[String, Double]): Double = {
    var res = 1.0
    for((k, va) <- v) {
      res = res * P(k, va)
    }
    return res
  }

  def binomial(x:Map[String, Double], mu: Double): Double = {
    val P_x = P(x)
    val m = featureVectors.size.toDouble
    return  C(m, mu) * scala.math.pow(P_x, mu) * scala.math.pow((1.0 - P_x), m - mu)
  }

  def pValue(x: Map[String, Double], mu0: Int) : Double = {
    var result = 0.0
    for(i <- 0 until featureVectors.size){
      result += binomial(x, i.toDouble)
    }
    return result
  }

  def floor(x: List[Map[String, Double]]) : Map[String, Double] = {
    var v: Map[String, Double] = Map()

    for(k <- ks){
      v += (k -> Double.MaxValue)
    }

    for(a <- x) {
      for (k <- ks) {
        if (a.getOrElse(k, Double.MaxValue) < v.getOrElse(k, Double.MaxValue)) {
          v += (k -> a.getOrElse(k, Double.MaxValue))
        }
      }
    }

    return v
  }

  def ceiling(x: List[Map[String, Double]]) : Map[String, Double] = {
    var v: Map[String, Double] = Map()

    for(k <- ks){
      v += (k -> Double.MinValue)
    }

    for (a <- x) {
      for (k <- ks) {
        if(a.getOrElse(k, 0.0) > v.getOrElse(k, 0.0)) {
          v += (k -> a.getOrElse(k, 0.0))
        }
      }
    }

    return v
  }

  def mine(x: Map[String, Double], S: List[Map[String, Double]], b: Int): Unit = {

    if(pValue(x, 0) <= maxPvalue) {
      A = A :+ x
    }

    for(i <- b until x.size) {
      var SPrime = List[Map[String, Double]]()
      for (v <- S) {
        if(v.getOrElse(ks(i), 0.0) > x.getOrElse(ks(i), 0.0)) {
          SPrime = SPrime :+ v
        }
      }

      if(SPrime.length >= (minSup).toInt) {
        val xPrime = floor(SPrime)
        var test = 1

        for(j <- 0 until i) {
          if(xPrime.getOrElse(ks(j), 0.0) > x.getOrElse(ks(j), 0.0)) {
            test = 0
          }
        }

        if(test == 1) {
          if(pValue(ceiling(SPrime), SPrime.length) < maxPvalue) {
            mine(xPrime, SPrime, i)
          }
        }

      }
    }
  }

  def getFVMineResult() : List[Map[String, Double]] = {
    return A
  }

  def run(x: Map[String, Double], S: List[Map[String, Double]], b: Int): List[Map[String, Double]] ={

    A = List[Map[String, Double]]()
    mine(x, S, b)

    return getFVMineResult()
  }

}
