/**
  * Created by mattieu on 31/03/16.
  */
class Edge(NodeStartArg: String, NodeEndArg: String, labelArg: Int) {
  val nodeStart: String = NodeStartArg
  val nodeEnd: String = NodeEndArg
  val label:Int = labelArg

  override def toString(): String = {
    return nodeStart + " " + nodeEnd + " " + label
  }
}
