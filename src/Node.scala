import util.Random
import scala.collection.mutable.Map

/**
  * Created by mattieu on 31/03/16.
  */

class Node(idArg: String, reachesArg: List[Node], labelArg: String, graphIdArg: String) {
  val id:String = idArg
  val label:String = labelArg
  val graphId:String = graphIdArg
  var reaches:List[Node] = reachesArg
  var arcs:List[Edge] = List()
  var featureVector:Map[String, Double] = Map()

  def addEdge(edge:Edge, node: Node): Unit = {
    reaches = reaches :+ node
    arcs = arcs :+ edge
  }

  def getReachableNodes(): List[Node] = {
    return reaches
  }

  def getFeatureVector(PRestart: Int): Array[Int] = {
    return Array[Int]()
  }

  def L1Dist (origProfile: Map[String, Double], profile: Map[String, Double], origSize: Int, currSize: Int): Double = {
    var diff:Double = 0.0
    if (origSize == 0) return 100.0
    else {
      for ((k,v) <- profile) {
        diff += Math.abs(origProfile.getOrElse(k, 0.0) / origSize - profile(k) / currSize)
      }
      return diff
    }
  }


  def approximate(profile: Map[String, Double], approx: Int): Map[String, Double] = {
    var appProfile:Map[String, Double] = Map()

    for((k,v) <- profile){
      appProfile += (k -> (scala.math.round(profile(k) * approx).toDouble))
      //if (appProfile(k) > 10) System.out.println(appProfile(k))

    }
    return appProfile
  }

  def randomWalk(Prestart: Double): Map[String, Double] = {
    if (reaches.length > 0) {
      var dist = 100.0
      var curr = this
      var previous = id
      var currSize = 1
      val convergence = 0.01
      while (dist > convergence) {
        var origProfile:Map[String, Double] = Map()
        for((k,v) <- featureVector) {
          origProfile += (k -> v)
        }

        val origSize = currSize
        for (j <- 0 until 10) {
          val r = Math.random()
          //var reachesI = List[Node]()
          val reachesI = curr.reaches
          /*if(curr.id == id) {
            reachesI = curr.reaches
          } else {
            reachesI = curr.reaches.filter(_.id != previous)
          }*/

          if (r <= Prestart) {
            curr = this
            previous = id
          } else if(reachesI.length > 0) {
            val index = Random.nextInt(reachesI.length)
            val labels = Array(curr.label, reachesI(index).label)
            val labels_sorted = labels.sorted
            val EdgeId = labels_sorted(0) + "-" + labels_sorted(1)

            val passage = featureVector.getOrElse(EdgeId, 0.0)
            if (passage == 0.0) {
              currSize += 1
            }

            featureVector += (EdgeId -> (passage + 1.0))
            previous = curr.id
            curr = reachesI(index)
          }
        }
        dist = L1Dist(origProfile, featureVector, origSize, currSize)
      }

      for ((k, v) <- featureVector) {
        featureVector += (k -> v/currSize.toDouble)
      }
      return approximate(featureVector, 10)
    }
    return featureVector
  }

}
