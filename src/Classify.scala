import scala.collection.mutable.Map
import scala.collection.mutable

/**
  * Created by mattieu on 2/04/16.
  */
class Classify(P: List[Map[String, Double]], N: List[Map[String, Double]], g: Graph, k: Int) {

  var delta = 0.01

  def classify(): Int =  {
    var PQ = mutable.PriorityQueue.empty[(Double, Int)]
    var score = 0.0

    for ((n, k) <- g.graph) {
      val node = g.graph(n)
      val vectorN = node.randomWalk(0.25)
      val posDist = minDist(vectorN, P)
      val negDist = minDist(vectorN, N)

      if (negDist < posDist) {
        PQ += (negDist -> -1)
      } else {
        PQ += (posDist -> 1)
      }
    }
    for ((k, v) <- PQ) {
      score += (1.0 / (k.toDouble + delta)) * v.toDouble
    }

    if (score > 0.0) {
      return 1
    } else {
      return -1
    }
  }

  def minDist(x: Map[String, Double], V: List[Map[String, Double]]): Double = {
    var min = Double.MaxValue
    for(v <- V) {
      val ksI = x.keys.toList ++ v.keys.toList
      val ks = ksI.toSet.toList
        if (incl(v, x, ks)) {
          var dist = 0.0

          for(k <- ks){
            dist += x.getOrElse(k, 0.0) - v.getOrElse(k, 0.0)
          }

          if(dist < min){
            min = dist
          }
        }
    }
    return min
  }

  def incl(x: Map[String, Double], y: Map[String, Double], ks: List[String]): Boolean = {
    var result = true

    for(k <- ks) {
      if(x.getOrElse(k, 0.0) > y.getOrElse(k, 0.0)) {
        result = false
        return result
      }
    }
    return result
  }

}
