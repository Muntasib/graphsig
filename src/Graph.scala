import javax.swing.JFrame

import com.mxgraph.swing.mxGraphComponent
import com.mxgraph.util.mxConstants
import com.mxgraph.view.mxGraph

import scala.collection.mutable.Map;

/**
  * Created by mattieu on 30/03/16.
  */
class Graph(graphIdArg: String, labelListArg: List[String], edgesArg: List[Edge], nodesIdArg: List[String]) {
  val graphId: String = graphIdArg
  val labelList: List[String] = labelListArg
  val edges: List[Edge] = edgesArg
  val nodesId: List[String] = nodesIdArg
  var graph: Map[String, Node] = Map()

  def buildGraph(): Unit = {
    for(i <- 0 until nodesId.length) {
      graph += (nodesId(i).toString -> new Node(nodesId(i), List[Node](), labelList(i), graphId))
    }

    for(edge <- edges) {
      graph(edge.nodeStart.toString).addEdge(edge,graph(edge.nodeEnd.toString))
    }
  }

  def getFeatureVectors(PRestart: Double): Map[String, Map[String, Double]] = {
    var RWR: Map[String, Map[String, Double]] = Map()

    for((k,v) <- graph) {
        val r = graph(k).randomWalk(PRestart)
        if(!r.isEmpty) {
          RWR += (k -> r)
        }
    }

    return RWR
  }

  def draw(): Unit ={
    val frame = new GraphFrame(graphId, graph, edges);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setSize(800, 600);
    frame.setVisible(true);
  }
}

private class GraphFrame(id:String, graph: Map[String, Node], edges: List[Edge]) extends JFrame("Graph : "+id){

  val mxgraph = new mxGraph()
  val fparent = mxgraph.getDefaultParent()
  val vertices:Map[String, Object] = Map()

  mxgraph.getModel().beginUpdate()
  try {
    var x:Int = 20
    var y:Int = 10
    for(g <- graph.keys){
      vertices += (g -> mxgraph.insertVertex(fparent, null, graph(g).label.toUpperCase(), x, y, 30, 30, mxConstants.STYLE_SHAPE + "="+mxConstants.SHAPE_ELLIPSE))
      if(x < y){
        x += 40
      } else{
        y += 40
      }
    }
    for(e <- edges){
      mxgraph.insertEdge(fparent, null, e.label, vertices(e.nodeStart), vertices(e.nodeEnd),mxConstants.STYLE_ENDARROW + "=" + mxConstants.NONE)
    }
  } finally {
    mxgraph.getModel().endUpdate()
  }

  val graphComponent = new mxGraphComponent(mxgraph)
  getContentPane().add(graphComponent)

}
