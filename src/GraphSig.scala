import java.io.{File, PrintWriter}
import scala.sys.process._
import scala.io.Source
import scala.collection.mutable.{Map, ListBuffer}

/**
  * Created by muntasib on 31/03/16.
  */

class GraphSig(G:Map[String, Graph], minSup:Double, maxPvalue:Double, PRestart: Double, radius:Int){

  var FV : Map[String, Map[String, Double]] = Map()
  var D : Map[String, List[String]] = Map()
  var A : List[Graph] = List()
  var nodes : Map[String, String] = Map()

  def run(): Unit ={
    var count = 0
    for(g <- G.values){
      count +=1
      for(k <- g.graph.keys.toList){
        nodes += (k -> g.graphId)
      }
      val r  = RWR(g, PRestart)
      FV = FV ++ r
      for(v <- r.keys){
        val a = g.graph(v).label
        if(! D.contains(a)){
          D += (a -> List[String]())
        }
        D(a) = D(a) :+ v
      }
    }

    count = 0
    for(a <- D.keys){
      count += 1
      var Da = List[Map[String, Double]]()
      for(k <- D(a)) {
        Da = Da :+ FV(k)
      }
      var input: Map[String, Map[String, Double]] = Map()
      count = 0
      for(x <- Da) {
        count += 1
        input += (count.toString -> x)
      }

      val fvmine = new FVMine(minSup,maxPvalue, input)
      val S = fvmine.run(floor(Da), Da, 0)
      var num:Int= 0
      for(v <- S){
        val N = nodes.filter((x) => G(x._2).graph(x._1).label == a && incl(v, vec(x._1)))
        var E = List[Graph]()
        for(n <- N.keys){
          E = E :+ cutGrahp(n,G(N(n)), radius, E.length)
        }
        maximalFSM(E, 0.8, a, num)
        num += 1
      }
    }

    writeGraph(A,"0",0)
  }

  def vec(idNode: String): Map[String, Double] = {
    if(FV.contains(idNode)){
      return FV(idNode)
    } else {
      return Map()
    }
  }

  def cutGrahp(n:String, g:Graph, radius:Int, id:Int): Graph ={
    var current_node:List[Node] = List()
    current_node = current_node :+ g.graph(n)

    def accumulate(hop:Int, reach: List[Node], nod:List[Node], ed:List[Edge]): Graph ={
      if(hop > radius){
        val ng = new Graph("#"+id, nod.map(n => n.label).toList, ed,nod.map(n => n.id).toList)
        ng.buildGraph()
        return ng
      }
      var nodes:List[Node] = List()
      var edges:List[Edge] = List()
      for(n <- reach){
        nodes = nodes ++ n.reaches
        edges = edges ++ n.arcs
      }
      return accumulate(hop+1, nodes, nod ++ nodes, ed ++ edges)
    }

    return accumulate(0,current_node,current_node,List[Edge]())
  }

  def maximalFSM(E : List[Graph], freq : Double, label:String, vector:Int): Unit ={

    writeGraph(E,label,vector)
    val cmd = "./gspan -f graphs -s "+freq.toString+" -o -i"
    val out = cmd.!

    readGspan()
  }

  def writeGraph(E : List[Graph],label:String, vector:Int): Unit ={
    val dir = new File("test")
    if(!dir.exists()){
      dir.mkdirs()
    }
    val writer = new PrintWriter(new File("test/graphs_"+label+"_"+vector.toString ))
    var count = 1
    val nodes:Map[String,String] = Map()
    for(g <- 0 until E.length){
      writer.write("t # "+g+"\n")
      for(v <- E(g).graph.values.toList){
        nodes += (v.id -> count.toString)
        count += 1
        writer.write("v "+nodes(v.id)+" "+v.label.getBytes()(0)+"\n")
      }
      for(e <- E(g).edges){
        writer.write("e "+nodes(e.nodeStart)+" "+nodes(e.nodeEnd)+" "+e.label+"\n")
      }
    }

    writer.close
  }

  def readGspan(): Unit ={
    var edges: ListBuffer[Edge] = ListBuffer[Edge]()
    var nodesId: ListBuffer[String] = ListBuffer[String]()
    var labelNode: ListBuffer[String] = ListBuffer[String]()
    for(line <- Source.fromFile("graphs.fp").getLines()){
      val content = line.split(" ")
      line match{
        case l if l.startsWith("t #") => {
          edges = ListBuffer[Edge]()
          nodesId = ListBuffer[String]()
          labelNode = ListBuffer[String]()
        }
        case l if l.startsWith("v ") => {
          nodesId += content(1)
          labelNode += content(2)
        }
        case l if l.startsWith("e ") =>{
          edges += new Edge(content(1),content(2),content(3).toInt)
        }
        case l if l.startsWith("x ") =>{
          val g = new Graph("#"+A.length,labelNode.toList, edges.toList, nodesId.toList)
          g.buildGraph()
          A = A :+ g
        }
        case _ => Unit
      }
    }
  }

  def RWR(g : Graph, PRestart: Double): Map[String, Map[String, Double]] = {
    return g.getFeatureVectors(PRestart)
  }

  def incl(x: Map[String, Double], y: Map[String, Double]): Boolean = {
    var result = true
    val ksI = x.keys.toList ++ y.keys.toList
    val ks = ksI.toSet.toList

    for(k <- ks) {
      if(x.getOrElse(k, 0.0) > y.getOrElse(k, 0.0)) {
        result = false
        return result
      }
    }
    return result
  }

  def floor(x: List[Map[String, Double]]) : Map[String, Double] = {
    var v: Map[String, Double] = Map()
    for(a <- x) {
      for (k <- a.keys) {
        if (a.getOrElse(k, Double.MaxValue) < v.getOrElse(k, Double.MaxValue)) {
          v += (k -> a.getOrElse(k, Double.MaxValue))
        }
      }
    }

    return v
  }

}
